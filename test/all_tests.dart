// Copyright (c) 2014, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_rest.all.tests;

import 'package:unittest/unittest.dart';
import 'resource_test.dart' as resource;
import 'resource_bind_test.dart' as resource_bind;

void main() {
  group('[resource]', resource.main);
  group('[resource_bind]', resource_bind.main);
}