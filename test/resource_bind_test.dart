// Copyright (c) 2014, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_rest.resource.bind.test;


import 'package:shelf_rest/src/util.dart';
import 'package:shelf_rest/src/resource.dart';
import 'package:shelf_rest/src/resource_bind.dart';
import 'package:shelf_rest/src/annotations.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf_path/shelf_path.dart';
import 'package:unittest/unittest.dart';
import 'dart:async';
import 'dart:convert';
import 'package:shelf_rest/src/handler.dart';
import 'package:constrain/constrain.dart';
import 'dart:core' hide Pattern;


main() {
  group('bindResource', () {
    runTests(String methodName, dynamic unboundResource, String httpMethod,
        RouteDetails chooseRoute(Resource r), {
          int expectedStatusCode: 200, String expectedBody: 'blah',
          String expectedLocation}) {
      group('produces a handler for a $methodName method', () {
        final Resource r = bindResource(unboundResource);

        RouteDetails route() => chooseRoute(r);
        Handler handler() => chooseRoute(r).handler;

        respFuture() {
            final h = handler();
            if (h == null) {
              throw new ArgumentError("no handler for method $httpMethod");
            }
            final request = new Request(httpMethod,
                            Uri.parse('http://localhost/blah'),
                            body: new Stream.fromIterable(
                                [JSON.encode({'foo': 'blah'}).codeUnits]));

            final requestWithPV = addPathParameters(request,
                {'resourceId': 'blah'});

            return h(requestWithPV);
        }

        test('which is not null', () {
          expect(handler(), isNotNull);
        });

        group('which when invoked', () {
          group('returns a future response', () {

            test('', () {
              expect(respFuture(), new isInstanceOf<Future<Response>>());
            });

            test('with a status code $expectedStatusCode', () {
              // TODO: rewrite to use completion and custom property matcher
              final rf = respFuture();
              rf.then((Response resp) {
                expect(resp.statusCode, equals(expectedStatusCode));
              });
              expect(rf, completes);
            });
            test('with expected body', () {
              final rf = respFuture();
              rf.then((Response resp) {
                resp.readAsString().then((body) {
                  expect(body, equals(expectedBody));
                });
              });
              expect(rf, completes);
            });
            test('with expected location header', () {
              final rf = respFuture();
              rf.then((Response resp) {
                expect(resp.headers['Location'], equals(expectedLocation));
              });
              expect(rf, completes);
            });
          });
        });
      });
    }

    group('', () {
      runTests('create', new OneResource(), 'POST', (r) => r.postRoute);
    });

    group('', () {
      runTests('update', new TwoResource(), 'PUT', (r) => r.putRoute);
    });

    group('', () {
      runTests('find', new ThreeResource(), 'GET', (r) => r.findRoute);
    });

    group('', () {
      runTests('delete', new FourResource(), 'DELETE', (r) => r.deleteRoute);
    });

    group('', () {
      runTests('search', new SixResource(), 'GET', (r) => r.searchRoute);
    });

    group('binds child resources', () {
      final Resource r = bindResource(new FiveResource());
      Resource firstChild() => r.childResources.values.first;

      test('and return non null object', () {
        expect(r.childResources, isNotNull);
      });

      test('and return map with correct number of children', () {
        expect(r.childResources, hasLength(1));
      });

      test('and return map with correct key', () {
        expect(r.childResources.keys.first, equals('/one'));
      });

      test('and return map with non null value', () {
        expect(firstChild(), isNotNull);
      });

      test('and bind handlers in child', () {
        expect(firstChild().postRoute, isNotNull);
      });
    });

    group('generates query parameters for search', () {
      test('which when no method parameters present is empty', () {
        final Resource r = bindResource(new SixResource());
        expect(r.searchQueryParameters, isEmpty);
      });

      group('which when some method parameters present', () {
        final Resource r = bindResource(new SevenResource());

        test('is not empty', () {
          expect(r.searchQueryParameters, isNot(isEmpty));
        });

        test('has correct number of parameters', () {
          expect(r.searchQueryParameters, hasLength(2));
        });

        test('has correct first parameter', () {
          expect(r.searchQueryParameters.first, equals('foo'));
        });

        test('has correct second parameter', () {
          expect(r.searchQueryParameters.elementAt(1), equals('bar'));
        });
      });

      group('which when both path and extra method parameters present', () {
        final Resource r = bindResource(new EightResource());

        test('is not empty', () {
          expect(r.searchQueryParameters, isNot(isEmpty));
        });

        test('does not include path parameter', () {
          expect(r.searchQueryParameters, hasLength(1));
        });

        test('does include non path parameter', () {
          expect(r.searchQueryParameters.first, equals('foo'));
        });
      });

      group('which when resource is a child of another resource', () {
        final Resource parentResource = bindResource(new NineResource());
        Resource r() => parentResource.childResources.values.first;

        test('is not empty', () {
          expect(r().searchQueryParameters, isNot(isEmpty));
        });

        test('does not include any path parameters', () {
          expect(r().searchQueryParameters, hasLength(1));
        });

        test('does include non path parameter', () {
          expect(r().searchQueryParameters.first, equals('foo'));
        });
      });
    });

    group('processes annotations on resource class and', () {
      runTests('create', new ElevenResource(), 'POST', (r) => r.postRoute);
    });

    group('allows annotations on resource method to change REST method and', () {
      runTests('create', new TwelveResource(), 'PUT', (r) => r.putRoute);
    });

    group('allows annotations on resource method to override default method name and', () {
      runTests('create', new ThirteenResource(), 'POST', (r) => r.postRoute);
    });

    group('supports setting of location header and', () {
      runTests('create', new FourteenResource(), 'POST', (r) => r.postRoute,
          expectedLocation: 'http://localhost/blah/xxx',
          expectedBody: '{"foo":"blah"}', expectedStatusCode: 201);
    });

    test('allows annotations on resource method to specify middleware', () {
      final handler = restHandler('/bar', new FifteenResource());
      request() => new Request('GET',
                      Uri.parse('http://localhost/bar/blah'));

      handle() => handler(request());

      expect(handle(), isNotNull);
      expect(handle().then((r) => r.context),
          completion(containsPair('testMiddleware', 'was here')));
    });

    group('supports validation settings', () {
      group('allowing defaults to be set on the bind function', () {
        Handler handler(bool validateParameters) =>
            restHandler('/bar', new FifteenResource(),
                validateParameters: validateParameters);
        request(String arg) => new Request('GET',
                        Uri.parse('http://localhost/bar/$arg'));

        handle(bool validateParameters, String arg) =>
            handler(validateParameters)(request(arg));

        test('and when validate parameters is false it doesnt validate', () {
          expect(handle(false, "---"), completes);
        });
        test('and when validate parameters is true it validates', () {
          expect(handle(true, "---"), throws);
        });
      });

      group('allowing overrides by annotation', () {
        Handler handler(bool validateParameters) =>
            restHandler('/bar', new SixteenResource(),
                validateParameters: validateParameters);
        request(String arg) => new Request('GET',
                        Uri.parse('http://localhost/bar/$arg'));

        handle(bool validateParameters, String arg) =>
            handler(validateParameters)(request(arg));

        test('and takes precedence over bind arguments', () {
          expect(handle(false, "---"), throws);
        });
      });

    });

  });
}

class One {
  final String foo;
  String id;

  One.build({this.foo});

  One.fromJson(Map json)
      : this.foo = json['foo'];

  Map toJson() => {
    'foo' : foo
  };

}

class OneResource {
  final String pathParameterName = 'foo';

  Response create(@RequestBody() One one) {
    return new Response.ok(one.foo);
  }
}

class TwoResource {
  final String pathParameterName = 'foo';

  Response update(@RequestBody() One one) {
    return new Response.ok(one.foo);
  }
}

class ThreeResource {
  final String pathParameterName = 'resourceId';

  Response find(String resourceId) {
    return new Response.ok(resourceId);
  }
}

class FourResource {
  final String pathParameterName = 'resourceId';

  Response delete(String resourceId) {
    return new Response.ok(resourceId);
  }
}

class FiveResource {
  final String pathParameterName = 'fum';

  Response find(String resourceId) {
    return new Response.ok(resourceId);
  }

  Map<dynamic, dynamic> childResources = {
    '/one': new OneResource()
  };
}

class SixResource {
  final String pathParameterName = 'resourceId';

  Response search(String resourceId) {
    return new Response.ok(resourceId);
  }
}

class SevenResource {
  final String pathParameterName = 'resourceId';

  Response search(String foo, String bar) {
    return new Response.ok('$foo,$bar');
  }
}

class EightResource {
  final String pathParameterName = 'resourceId';

  Response search(String resourceId, String foo) {
    return new Response.ok('$resourceId, $foo');
  }
}

class NineResource {
  final String pathParameterName = 'resourceId9';

  Map<dynamic, dynamic> childResources = {
    '/ten': new TenResource()
  };

}

class TenResource {
  final String pathParameterName = 'resourceId10';

  Response search(String resourceId9, String resourceId10, String foo) {
    return new Response.ok('');
  }
}

@RestResource('foo')
class ElevenResource {
  Response create(@RequestBody() One one) {
    return new Response.ok(one.foo);
  }
}

@RestResource('foo')
class TwelveResource {
  @ResourceMethod(method : 'PUT')
  Response create(@RequestBody() One one) {
    return new Response.ok(one.foo);
  }
}

@RestResource('foo')
class ThirteenResource {
  @ResourceMethod(operation: RestOperation.CREATE)
  Response manufacture(@RequestBody() One one) {
    return new Response.ok(one.foo);
  }
}

@RestResource('foo')
class FourteenResource {
  One create(@RequestBody() One one) {
    one.id = 'xxx';
    return one;
  }
}


@RestResource('foo')
class FifteenResource {
  @ResourceMethod(middleware: _testMiddleware)
  Response find(@Pattern(r'[\w]+') String foo) {
    return new Response.ok(foo);
  }
}

_testMiddleware() => testMiddleware;

Handler testMiddleware(Handler inner) {
  return (Request request) {
    final responseFuture = syncFuture(() => inner(request));
    return responseFuture.then((response) =>
      response.change(context: {'testMiddleware': 'was here' }));
  };
}

@RestResource('foo')
class SixteenResource {
  @ResourceMethod(validateParameters: true, validateReturn: true)
  Response find(@Pattern(r'[\w]+') String foo) {
    return new Response.ok(foo);
  }
}
