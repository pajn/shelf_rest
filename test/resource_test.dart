// Copyright (c) 2014, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_rest.resource.test;


import 'package:shelf_rest/src/resource.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf_route/shelf_route.dart';
import 'package:unittest/unittest.dart';
import 'package:mock/mock.dart';

class MockRouter extends Mock implements Router {
  noSuchMethod(Invocation invocation) => super.noSuchMethod(invocation);
}

main() {
  group('createRoutes', () {
    MockRouter router;
    MockRouter childRouter;
    MockRouter grandChildRouter;

    testHandler(Request request) => new Response.ok('all good');
    final testDetails = new RouteDetails(testHandler);

    setUp(() {
      router = new MockRouter();
      childRouter = new MockRouter();
      grandChildRouter = new MockRouter();
      router.when(callsTo('child')).alwaysReturn(childRouter);
      childRouter.when(callsTo('child')).alwaysReturn(grandChildRouter);
    });

    skip_test('should throw ArgumentError if null path parameter', () {
      expect(() => (new TestResource()
        ..putRoute = testDetails)
        .createRoutes(router),
          throwsArgumentError);
    });

    test('should NOT create routes if NO handler present', () {
      new TestResource('fred')..createRoutes(router);
      router.verifyZeroInteractions();
    });

    test('should create search route if handler present', () {
      new TestResource('fred')
        ..searchRoute = testDetails
        ..createRoutes(router);
      router.calls('add', '/', ['GET'], equals(testHandler)).verify(happenedOnce);
      childRouter.verifyZeroInteractions();
    });

    test('should include query parameters in search route if present', () {
      new TestResource('fred')
        ..searchRoute = testDetails
        ..searchQueryParameters = ['foo', 'bar']
        ..createRoutes(router);
      router.calls('add', '{?foo,bar}', ['GET'],
          equals(testHandler)).verify(happenedOnce);
      childRouter.verifyZeroInteractions();
    });

    test('should create post route if handler present', () {
      new TestResource('fred')
        ..postRoute = testDetails
        ..createRoutes(router);
      router.calls('add', '/', ['POST'], equals(testHandler)).verify(happenedOnce);
      childRouter.verifyZeroInteractions();
    });

    test('should create find route if handler present', () {
      new TestResource('fred')
        ..findRoute = testDetails
        ..createRoutes(router);
      router.calls('child','/{fred}').verify(happenedOnce);
      childRouter.calls('add', '/', ['GET'], equals(testHandler)).verify(happenedOnce);
    });

    test('should create put route if handler present', () {
      new TestResource('fred')
        ..putRoute = testDetails
        ..createRoutes(router);
      router.calls('child','/{fred}').verify(happenedOnce);
      childRouter.calls('add', '/', ['PUT'], equals(testHandler)).verify(happenedOnce);
    });

    test('should create delete route if handler present', () {
      new TestResource('fred')
        ..deleteRoute = testDetails
        ..createRoutes(router);
      router.calls('child','/{fred}').verify(happenedOnce);
      childRouter.calls('add', '/', ['DELETE'], equals(testHandler)).verify(happenedOnce);
    });

    test('should add routes in child resource', () {
      final childResource = new TestResource('barney')
        ..searchRoute = testDetails;

      new TestResource('fred')
        ..childResources = { '/barney': childResource }
        ..createRoutes(router);

      router.calls('child','/{fred}').verify(happenedOnce);
      childRouter.calls('child','/barney').verify(happenedOnce);
      grandChildRouter.calls('add', '/', ['GET'],
          equals(testHandler)).verify(happenedOnce);
    });
  });
}


class TestResource extends Resource {
  @override
  String pathParameterName;

  @override
  RouteDetails searchRoute;

  @override
  RouteDetails findRoute;

  @override
  RouteDetails postRoute;

  @override
  RouteDetails putRoute;

  @override
  RouteDetails deleteRoute;

  @override
  Iterable<String> searchQueryParameters = const [];

  TestResource([this.pathParameterName]);

  @override
  Map<dynamic, Resource> childResources = const {};

}

