// Copyright (c) 2014, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_rest.handler;

import 'package:shelf/shelf.dart';
import 'package:shelf_route/shelf_route.dart';

import 'resource_bind.dart' show bindResource;
export 'resource_bind.dart' show bindResource;

Handler restHandler(dynamic path, dynamic resource,
                    { bool validateParameters: true,
                      bool validateReturn: false }) =>
    restRouter(path, resource, validateParameters: validateParameters,
        validateReturn: validateReturn).handler;


Router restRouter(dynamic path, dynamic resource,
                  { bool validateParameters: true,
                    bool validateReturn: false }) =>
    router()..addAll(bindResource(resource,
        validateParameters: validateParameters,
        validateReturn: validateReturn), path: path);



