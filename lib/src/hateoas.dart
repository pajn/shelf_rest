// Copyright (c) 2014, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_rest.hateoas;

import 'preconditions.dart';
import 'package:option/option.dart';
import 'package:uri/uri.dart';

class Link {
  final String rel;
  final String href;
  Uri get hrefAsUri => Uri.parse(href);
  UriTemplate get hrefAsUriTemplate => new UriTemplate(href);

  Link(String rel, String href)
      : this.rel = checkNotNull(rel),
        this.href = checkNotNull(href);

  Link.uri(String rel, Uri href)
      : this(rel, href.toString());

  Link.uriTemplate(String rel, UriTemplate href)
      : this(rel, href.template);

  Link.self(Uri href) : this.uri('self', href);

  Link.fromJson(Map<String, Object> json)
      : this(json['rel'], json['href']);

  Map<String, Object> toJson() => {
    'rel': rel,
    'href': href
  };

  @override
  int get hashCode => href.hashCode;

  @override
  bool operator==(other) =>
      other is Link && other.rel == rel && other.href == href;

  @override
  String toString() => 'Link($rel : $href)';
}

abstract class Identified {
  String get id;
}

class SearchResult<R> {
  final int totalCount;
  final int page;
  final List<R> results;
  final int numPages;
  int get pageSize => numPages > 0 ? (totalCount / numPages).ceil() : 1;

  SearchResult(int totalCount, int numPages, int page,
      this.results)
      : this.totalCount = checkNotNull(totalCount),
        this.numPages = checkNotNull(numPages),
        this.page = checkNotNull(page) {
    ensure(totalCount, greaterThanOrEqualTo(0));
    ensure(numPages, greaterThanOrEqualTo(0));
    ensure(page, greaterThanOrEqualTo(0));
  }

  SearchResult.fromJson(Map json, R resourceFromJson(Map resourceJson))
      : this(
          json['totalCount'],
          json['numPages'],
          json['page'],
          (json['results'] as Iterable).map(resourceFromJson)
            .toList(growable: false)
          );


  Map toJson() => {
    'totalCount': totalCount,
    'numPages': numPages,
    'page': page,
    'results': results.map((r) => r.toJson()).toList(growable: false)
  };
}

class SearchResourceModel<R> extends SearchResult<R> {
  final List<Link> links;
  final Uri self;
  final Option<Uri> previousPage;
  final Option<Uri> nextPage;

  SearchResourceModel(int totalCount, int numPages, int page, List<R> results,
      Uri self, { Uri previousPage, Uri nextPage })
      : this.self = checkNotNull(self),
        this.previousPage = new Option(previousPage),
        this.nextPage = new Option(nextPage),
        links = (new LinkHelper(self)
          ..addLink('previousPage', previousPage)
          ..addLink('nextPage', nextPage))
          .links,
        super(totalCount, numPages, page, results);

  SearchResourceModel.fromResult(SearchResult<R> result,
      Uri self, { Uri previousPage, Uri nextPage })
      : this(result.totalCount, result.numPages, result.page, result.results,
          self, previousPage: previousPage, nextPage: nextPage);

  factory SearchResourceModel.fromJson(Map json,
      R resourceFromJson(Map resourceJson)) {

    final helper = new LinkJsonHelper(json);

    return new SearchResourceModel.fromResult(
        new SearchResult.fromJson(json, resourceFromJson),
            helper.uri('self'),
            previousPage: helper.uri('previousPage'),
            nextPage: helper.uri('nextPage'));
  }

  Map toJson() =>
    super.toJson()..addAll({
      'links': links.map((l) => l.toJson()).toList(growable: false)
  });
}



class LinkJsonHelper {
  List<Link> links;

  LinkJsonHelper(Map json) {
    final linkJson = (json['links'] as List);

    links = linkJson.map((link) =>
      new Link.fromJson(link)).toList();
  }

  _link(String rel, convert(Link link)) {
    final result = links.where((l) => l.rel == rel);
    return result.isEmpty ? null : convert(result.first);
  }

  Uri uri(String rel) => _link(rel, (link) => link.hrefAsUri);

  UriTemplate uriTemplate(String rel) =>
      _link(rel, (link) => link.hrefAsUriTemplate);

}


class LinkHelper {
  final List<Link> links;

  LinkHelper([self]) :
    links = self != null ? [new Link.self(self)] : [];

  void addLink(String rel, href) {
    if (href != null) {
      links.add(new Link(rel, href.toString()));
    }
  }

}