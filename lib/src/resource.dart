// Copyright (c) 2014, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_rest.resource;

import 'package:shelf/shelf.dart';
import 'package:shelf_route/shelf_route.dart' show Routeable, Router;

abstract class Resource<T> implements Routeable {
  String get pathParameterName;

  String get _pathParamExpression => pathParameterName != null ?
      '/{$pathParameterName}' : null;

  RouteDetails get searchRoute => null;
  RouteDetails get findRoute => null;
  RouteDetails get postRoute => null;
  RouteDetails get putRoute => null;
  RouteDetails get deleteRoute => null;

  // TODO: do we want to support query params for other methods?
  Iterable<String> get searchQueryParameters => const [];

  Map<dynamic, Resource> get childResources => const {};

  @override
  void createRoutes(Router router) {
    // TODO: add integration tests that test 3 resources deep

    // TODO: search handler needs query params in the route
    // router.get('{?foo,bar}', searchHandler);
    final searchPath = searchQueryParameters.isNotEmpty ?
        '{?${searchQueryParameters.join(",")}}' : '/';

    _addRoute(router, searchPath, 'GET', searchRoute);
    _addRoute(router, '/', 'POST', postRoute);

    final pathParamRouter = _pathParamExpression != null ?
        router.child(_pathParamExpression) : router;

    _addRoute(pathParamRouter, '/', 'GET', findRoute);
    _addRoute(pathParamRouter, '/', 'PUT', putRoute);
    _addRoute(pathParamRouter, '/', 'DELETE', deleteRoute);

    childResources.forEach((path, resource) {
      final childResourceRouter = pathParamRouter.child(path);
      resource.createRoutes(childResourceRouter);
    });
  }

  void _addRoute(Router router, String path, String method, RouteDetails route) {
    if (route != null) {
      router.add(path, [method], route.handler, middleware: route.middleware);
    }

  }

}

class RouteDetails {
  final handler;
  final Middleware middleware;

  RouteDetails(this.handler, { this.middleware });
}

