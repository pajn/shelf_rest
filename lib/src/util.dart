library shelf_bind.util;

import 'dart:async';

import 'package:stack_trace/stack_trace.dart';

/// Like [Future.sync], but wraps the Future in [Chain.track] as well.
Future syncFuture(callback()) => Chain.track(new Future.sync(callback));


Iterable<Map> iterableToJson(Iterable i) =>
    i == null ? null : i.map((j) => j.toJson()).toList(growable: false);