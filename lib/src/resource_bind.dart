// Copyright (c) 2014, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_rest.resource.bind;

import 'package:shelf_bind/shelf_bind.dart';
export 'package:shelf_bind/shelf_bind.dart';
import 'package:shelf_rest/src/annotations.dart';
import 'dart:mirrors';
import 'resource.dart';
import 'preconditions.dart';
import 'dart:io';

Resource bindResource(dynamic resource,
                      { bool validateParameters: true,
                        bool validateReturn: false }) {
  return new _BoundResource(resource,
      validateParameters: validateParameters,
      validateReturn: validateReturn);
}

class _BoundResource extends Resource {
  final String pathParameterName;
  final RouteDetails searchRoute;
  final RouteDetails findRoute;
  final RouteDetails postRoute;
  final RouteDetails putRoute;
  final RouteDetails deleteRoute;
  final Iterable<String> searchQueryParameters;
  final Map<dynamic, Resource> childResources;

  _BoundResource._internal(this.pathParameterName,
      this.postRoute, this.deleteRoute,
      this.findRoute, this.searchRoute, this.putRoute,
      this.searchQueryParameters,
      this.childResources);

  factory _BoundResource(dynamic resource,
      { Iterable<String> parentPathParameters: const [],
        bool validateParameters: true, bool validateReturn: false }) {
    final resourceMirror = reflect(resource);
    final pathParameterName = _getPathParameterName(resourceMirror);
    final routes = _buildRoutes(resourceMirror,
        validateParameters, validateReturn);

    RouteDetails _route(RestOperation op) {
      final h = routes[op];
      return h != null ? h.route : null;
    }

    RouteDetails _routeIf(_RoutePair route, String method) {
      return route != null && route.method == method ?
              route.route : null;
    }

    RouteDetails _routeFor(_RoutePair primaryRoute,
                       _RoutePair secondaryRoute, String method) {
      final primary = _routeIf(primaryRoute, method);
      return primary != null ? primary :
        _routeIf(secondaryRoute, method);
    }

    final createRoutePair = routes[RestOperation.CREATE];
    final updateRoutePair = routes[RestOperation.UPDATE];

    final postRoute = _routeFor(createRoutePair,
        updateRoutePair, 'POST');
    final putRoute = _routeFor(updateRoutePair,
        createRoutePair, 'PUT');

    final deleteRoute = _route(RestOperation.DELETE);
    final findRoute = _route(RestOperation.FIND);
    final searchRoute = _route(RestOperation.SEARCH);

    if (pathParameterName == null
        && (putRoute != null
          || deleteRoute != null
          || findRoute != null)) {
      throw new ArgumentError(
          "If either a PUT, DELETE or FIND route defined then "
          "must specify the path parameter name for the resource. "
          "Either use the RestResource annotation or add a field called "
          "pathParameterName");
    }

    final pathParameters = pathParameterName != null ?
        (new List()
          ..addAll(parentPathParameters)
          ..add(pathParameterName))
        : parentPathParameters;

    final searchQueryParameters = _extractQueryParams(resourceMirror, 'search',
        pathParameters);
    final childResources = _createChildResources(resourceMirror, pathParameters,
        validateParameters, validateReturn);

    return new _BoundResource._internal(pathParameterName,
        postRoute, deleteRoute,
        findRoute, searchRoute, putRoute, searchQueryParameters,
        childResources);

  }

}


String _getPathParameterName(InstanceMirror resourceMirror) {
  final pathParamaterNameFromAnnotation =
      _getPathParameterNameFromAnnotation(resourceMirror);
  if (pathParamaterNameFromAnnotation != null) {
    return pathParamaterNameFromAnnotation;
  }

  try {
    final fieldMirror = resourceMirror.getField(#pathParameterName);
    return fieldMirror != null ? fieldMirror.reflectee : null;
  } on NoSuchMethodError {
//    throw new ArgumentError(
//        "Must specify the path parameter name for the resource. "
//        "Either use the RestResource annotation or add a field called "
//        "pathParameterName");
    return null;
  }
}

String _getPathParameterNameFromAnnotation(InstanceMirror resourceMirror) {
  final RestResource resourceAnnotation =
      _getAnnotation(resourceMirror.type, RestResource);

  return resourceAnnotation != null ? resourceAnnotation.pathParameterName
      : null;
}

dynamic _getAnnotation(DeclarationMirror dm, Type annotationType) {
  final resourceAnnotations = dm.metadata.where(
      (md) => reflectType(annotationType).isAssignableTo(md.type));
  if (resourceAnnotations.isEmpty) {
    return null;
  }

  return resourceAnnotations.first.reflectee;
}

class _RoutePair {
  final RouteDetails route;
  final String method;

  _RoutePair(this.route, this.method);
}

Map<RestOperation, _RoutePair> _buildRoutes(InstanceMirror resource,
    bool validateParameters, bool validateReturn) {
  final methods = _getMethodMirrors(resource);

  final routes = <RestOperation, _RoutePair>{};

  methods.forEach((s, mm) {
    final ResourceMethod resourceMethodAnnotation =
        _getAnnotation(mm, ResourceMethod);

    final specifiedOperation = (resourceMethodAnnotation != null &&
        resourceMethodAnnotation.operation != null) ?
            resourceMethodAnnotation.operation :
              null;

    final RestOperation operation = specifiedOperation != null ?
        specifiedOperation :
          RestOperation.defaultOperationForMethodName(
              MirrorSystem.getName(mm.simpleName));

    if (routes.containsKey(operation)) {
      throw new ArgumentError("More than one method maps to operation $operation."
          " Error occured while processing method ${mm.simpleName}");
    }

    if (operation != null) {
      final route = _buildRoute(resource, operation, mm,
          resourceMethodAnnotation, validateParameters, validateReturn);
      final httpMethod = (resourceMethodAnnotation != null
            && resourceMethodAnnotation.method != null) ?
          resourceMethodAnnotation.method : operation.defaultHttpMethod;
      routes[operation] = new _RoutePair(route, httpMethod);
    }
  });

  return routes;
}

RouteDetails _buildRoute(InstanceMirror resource, RestOperation operation,
    MethodMirror mm, ResourceMethod resourceMethodAnnotation,
    bool defaultValidateParameters, bool defaultValidateReturn) {

  final ClosureMirror closureMirror = resource.getField(mm.simpleName);

  final responseBinding = operation.responseHeaders.successStatus
      == HttpStatus.CREATED ?
      new JsonResponseBinding.created() : null; // TODO: get rid of created ctr?

  final middleware = resourceMethodAnnotation != null &&
        resourceMethodAnnotation.middleware != null ?
      resourceMethodAnnotation.middleware() : null;

  final validateParameters = resourceMethodAnnotation != null &&
      resourceMethodAnnotation.validateParameters != null ?
      resourceMethodAnnotation.validateParameters : defaultValidateParameters;

  final validateReturn = resourceMethodAnnotation != null &&
      resourceMethodAnnotation.validateReturn != null ?
      resourceMethodAnnotation.validateReturn : defaultValidateReturn;

  return new RouteDetails(
      // TODO: would be nice to leave the bind call to a handlerAdapter installed
      // into shelf route
      bind(closureMirror.reflectee, responseBinding: responseBinding,
           validateParameters: validateParameters,
           validateReturn: validateReturn),
      middleware: middleware);
}

MethodMirror _getMethodMirror(InstanceMirror resource, String methodName) {
  final dm = resource.type.declarations[new Symbol(methodName)];
  if (dm == null) {
    return null;
  }
  if (dm is MethodMirror && dm.isRegularMethod) {
    return dm;
  }

  return null;
}

Map<Symbol, MethodMirror> _getMethodMirrors(InstanceMirror resource) {
  final methods = <Symbol, MethodMirror>{};

  resource.type.declarations.forEach((k, v) {
    if (v is MethodMirror && v.isRegularMethod) {
      methods[k] = v;
    }
  });

  return methods;
}

Map<dynamic, Resource> _createChildResources(InstanceMirror resourceMirror,
    Iterable<String> pathParameters,
    bool validateParameters, bool validateReturn) {
  if (!resourceMirror.type.declarations.containsKey(#childResources)) {
    return const {};
  }

  final fieldMirror = resourceMirror.getField(#childResources);
  if (fieldMirror == null) {
    return const {};
  }

  ensure(fieldMirror.reflectee, new isInstanceOf<Map>());

  Map childMap = fieldMirror.reflectee;

  final childResources = <dynamic, Resource>{};

  childMap.forEach((path, resource) {
    childResources[path] = new _BoundResource(resource,
        parentPathParameters: pathParameters,
        validateParameters: validateParameters,
        validateReturn: validateReturn);
  });

  return childResources;


}

Iterable<String> _extractQueryParams(InstanceMirror resourceMirror,
    String methodName, Iterable<String> pathParameters) {
  var mm = _getMethodMirror(resourceMirror, methodName);
  if (mm == null) {
    return const [];
  }

  // exclude path parameters
  return mm.parameters.map((pm) => MirrorSystem.getName(pm.simpleName))
      .where((name) => !pathParameters.contains(name))
      .toList();
}

