// Copyright (c) 2014, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_rest;

export 'src/handler.dart';
export 'src/annotations.dart';
export 'src/hateoas.dart';

